import numpy as np
import pandas as pd
from pandas.util.testing import assert_frame_equal
import numpy.testing as npt
from carbonmodel import model
import unittest2

class TestModel(unittest2.TestCase):
    
    def setUp(self):
        names = ["a", "b", "c"]
        pools = {name: model.CarbonPool((3,)) for name in names}
        self.carbon = np.tile(1000,3)
        self.partitioning = {"a": 1.0}
        self.model = model.Model(pools)

    def test_initial_partition(self):
        self.setUp()
        self.model.allocate_initial_carbon(self.carbon, self.partitioning)
        npt.assert_equal(self.model.carbon_pools["a"].carbon, self.carbon)

    def test_reallocate_carbon(self):
        self.setUp()
        self.model.allocate_initial_carbon(self.carbon)
        npt.assert_equal(self.model.carbon_pools["a"].carbon, self.carbon / (1.0 * len(self.model.carbon_pools)))
        reallocation = {"a": np.tile(100, 3)}
        self.model.reallocate_carbon(reallocation)
        npt.assert_equal(self.model.carbon_pools["a"].carbon, self.carbon / (1.0 * len(self.model.carbon_pools)) + 100)

class TestCarbonPool(unittest2.TestCase):

    def setUp(self):
        carbon = np.tile(10,3)
        self.pool = model.CarbonPool((3,))
        self.pool.input_carbon(carbon)

    def test_input_carbon(self):
        self.setUp()
        npt.assert_equal(self.pool.carbon.values, np.tile(10,(3,1)))

    def test_output_carbon(self):
        self.setUp()
        self.pool.output_carbon(np.tile(10,3))
        assert_frame_equal(self.pool.carbon, pd.DataFrame({"base": np.tile(0.,3)}))

        self.setUp()
        output = pd.DataFrame({'base':np.array([0.1, 1, 3.1])})
        self.pool.output_carbon(output)
        assert_frame_equal(self.pool.carbon,
                           pd.DataFrame({"base": np.array([9.9, 9, 6.9])}))

    def test_moisture_effect(self):
        self.setUp()
        precip = np.array([10, 20, 100, 10])
        pet = np.array([10, 40, 20, 0])
        moisture_mult = self.pool.moisture_effect(precip, pet)
        print moisture_mult
        self.assertTrue(np.allclose(moisture_mult, np.array([1.00, 0.55, 1.00, 0.1])))

    def test_temp_effect(self):
        self.setUp()
        avg_temps = np.arange(10,60,10)
        temp_effects = self.pool.temp_effect(avg_temps)
        self.assertTrue(np.allclose(np.round(temp_effects,3),
            np.array([0.444, 0.667, 1, 1, 1])))

    def test_lignin_multiplier(self):
        self.setUp()
        no_lignin = self.pool.get_lignin_effect()
        npt.assert_allclose(no_lignin, np.ones(self.pool.carbon.shape))

        self.setUp()
        self.pool.percent_lignin = 0.5
        self.assertEqual(self.pool.percent_lignin, 0.5)

        some_lignin = self.pool.get_lignin_effect()
        npt.assert_allclose(some_lignin, np.exp(-3*np.tile(0.5, 3)))

    def test_initial_carbon_input(self):
        carbon = np.tile(10,10)
        pool = model.CarbonPool(carbon.shape)
        pool.input_carbon(carbon)
        npt.assert_equal(pool.carbon, carbon)
    
    def test_additional_carbon_input(self):
        self.setUp()
        expected = pd.DataFrame({"base": np.tile(10., 3)})
        assert(isinstance(self.pool.carbon, pd.DataFrame))

        assert_frame_equal(self.pool.carbon, expected)

        self.pool.input_carbon(np.tile(5, 3))
        expected = pd.DataFrame({"base":np.tile(15., 3)})
        assert_frame_equal(self.pool.carbon, expected)

    def test_run(self):
        self.setUp()
        phony_driver = (1,1,1)
        carbon_mult = 0.5
        result = self.pool.run(phony_driver, carbon_multiplier=carbon_mult)
        self.assertIn("atmosphere", result)
        npt.assert_equal(result['atmosphere']['base'].values, np.tile(2.75,3))

    def test_empty_pools(self):
        pool = model.CarbonPool((10,))
        pool.carbon = pd.DataFrame({"a": pool.carbon})
        carbon = pd.DataFrame({"a": np.arange(10), "b": np.arange(10)})
        pool.input_carbon(carbon)
        c_out = pool.run((1,1,1), carbon_multiplier=0.5)
        empty_pools = pool.stabilize(threshold=0.5)
        c_out['atmosphere'] = c_out['atmosphere'].add(empty_pools, fill_value=0)
        expected = pd.DataFrame({"a": np.arange(10), "b": np.arange(10)}) * 0.5 * 0.55
        expected.ix[1] += 0.5
        assert_frame_equal(c_out['atmosphere'], expected)

    def test_set_decay_rate(self):
        decay_rate = 0.1
        pool = model.CarbonPool((3,), max_decay_rate=decay_rate)
        self.assertEqual(pool.max_decay_rate, decay_rate)

class TestSlowCarbon(unittest2.TestCase):

    def setUp(self):
        carbon = np.tile(100,3)
        self.sc_pool = model.SlowCarbon(carbon.shape)
        self.sc_pool.input_carbon(carbon)

    def test_decay_rate(self):
        """
        this test exists to test a bug where this sometimes didnt get set...
        """
        self.setUp()
        self.assertTrue(self.sc_pool.max_decay_rate == 0.0165)
        self.setUp() # Test doesnt change -- used to ahve some crazy variation across runs
        self.assertTrue(self.sc_pool.max_decay_rate == 0.0165)

class TestCWD(unittest2.TestCase):

    def setUp(self):
        carbon = np.tile(100,3)
        self.pool = model.CoarseWoodyDebris((3,), 0.45)
        self.pool.input_carbon(carbon)

class TestNPP(unittest2.TestCase):

    
