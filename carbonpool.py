import calendar
from pandas.util.testing import set_trace
import pandas as pd
import random
import traceback
import glob
from os import path
import time
import numpy as np
import json
import logging

DEFAULT_AMOS = 0.55
SILT_FRACTION = 0.66 
PERCENT_CLAY = 0.8

logger = logging.getLogger("logger")
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
logger.addHandler(ch)

class CarbonPool(object):
    max_decay_rate = 0 # monthly 
    percent_lignin = 0
    driver_data = {}

    def __init__(self, shape, max_decay_rate=None):
        self.partition_rules = {}
        self.partitioning_rules()
        # TODO: this is a slight problem because no guarantee convert to df
        self.carbon = np.zeros(shape)
        if max_decay_rate:
            self.max_decay_rate = max_decay_rate

    def input_carbon(self, carbon):
        if isinstance(self.carbon, np.ndarray):
            self.carbon += carbon
        else:
            try:
                self.carbon = self.carbon.add(carbon, fill_value=0)
            except AssertionError:
                self.carbon['base'] += carbon
            except:
                traceback.print_exc()

    def output_carbon(self, carbon):
        if isinstance(self.carbon, np.ndarray):
            self.carbon -= carbon
        else:
            try:
                self.carbon = self.carbon.sub(carbon, fill_value=0)
            except AssertionError:
                self.carbon['base'] -= carbon
            except:
                traceback.print_exc()

    def run(self, drivers, q10=1.5, carbon_multiplier=None,
            SPINUP=False):
        # TODO: shouldnt have dependency on a global var
        try:
            if not carbon_multiplier:
                carbon_multiplier = (self.max_decay_rate *
                                    self.abiotic_multiplier(drivers, q10) *
                                    self.get_lignin_effect())

            if SPINUP:
                total_carbon_out = self.carbon * carbon_multiplier
            else:
                total_carbon_out = self.carbon.mul(carbon_multiplier, axis=0)
                empty_pools = self.stabilize()
                total_carbon_out.add(empty_pools, fill_value=0)

            carbon_out_by_pool = {}
            for output_pool, percent_output in self.partition_rules.iteritems():
                if SPINUP:
                    carbon_out = total_carbon_out * percent_output
                else:
                    carbon_out = total_carbon_out.mul(percent_output, axis=0)
                carbon_out_by_pool[output_pool] = carbon_out

            self.output_carbon(total_carbon_out)

            return carbon_out_by_pool
        except:
            traceback.print_exc()
            print "no carbon in pool yet"
            
    
    def stabilize(self, threshold=0.01):
        """ Round off pool when falls below a preset threshold
        For purposes of amazon droought, 0.01 represents a 10kg thresh
        """
        changes = self.carbon.where(self.carbon <= threshold, other=0)
        return changes

    def get_lignin_effect(self):
        """ Effect of lignin on decomp rates

        Taken from Parton et al 1993 GBC
        """
        self.lignin_effect = np.exp(-3*self.percent_lignin)
        return self.lignin_effect

    def abiotic_multiplier(self, drivers, q10):
        temp, precip, pet = drivers
        moisture_effect = self.moisture_effect(precip, pet)
        temp_effect = self.temp_effect(temp, q10)
        return moisture_effect * temp_effect
        
    def partitioning_rules(self):
        """ Specify to what pools C moves 
        """
        self.partition_rules["active"] = 0.45
        self.partition_rules["atmosphere"] = 0.55

    def moisture_effect(self, precip, pet):
        """ Effect of soil moisture on decomp

        Adapated from Parton et al 1984 where the funcitonal form is 
        not entirely spelled out
        """
        precip_pet_ratio = 0.1 + 0.9*((1.0 * precip) / pet) # this nugget is from CASA
        precip_pet_ratio[np.isinf(precip_pet_ratio)] = 0

        precip_pet_ratio[precip_pet_ratio > 1] = 1
        precip_pet_ratio[precip_pet_ratio < 0.1] = 0.1

        return precip_pet_ratio
    
    def temp_effect(self, temp, q10):
        """ Temperature effect on decompoistion
        """
        effect = lambda x: np.minimum(1, q10**((x - 30) / 10.0))
        return effect(temp)

class RootStructural(CarbonPool):
    max_decay_rate = 0.3297
    percent_lignin = 0.56

    def partitioning_rules(self):
        self.partition_rules['slow'] = 0.7
        self.partition_rules['atmosphere'] = 0.3

class RootMetabolic(CarbonPool):
    max_decay_rate = 0.786

class AboveStructural(CarbonPool):
    max_decay_rate = 0.2775 
    percent_lignin = 0.56

    def partitioning_rules(self):
        self.partition_rules['above_microbe'] = (1-self.percent_lignin)*0.4  
        self.partition_rules["slow"] = self.percent_lignin*0.7
        self.partition_rules["atmosphere"] = (1-self.percent_lignin)*0.6 + \
                                              self.percent_lignin*0.3

class AboveMetabolic(CarbonPool):
    max_decay_rate = 0.7087

    def partitioning_rules(self):
        self.partition_rules["above_microbe"] = 0.4
        self.partition_rules["atmosphere"] = 0.6

class LivingWood(CarbonPool):
    max_decay_rate = 0.00208116

    def abiotic_multiplier(self, drivers, q10):
        return 1

    def partitioning_rules(self):
        self.partition_rules["cwd"] = 1

class LivingRoot(CarbonPool):
    max_decay_rate = 0.04081053

    def abiotic_multiplier(self, drivers, q10):
        return 1

    def partitioning_rules(self):
        self.partition_rules["root_structural"] = 1 - 0.45
        self.partition_rules["root_metabolic"] = 0.45
    

class LivingLeaf(CarbonPool):
    max_decay_rate = 0.04081053

    def abiotic_multiplier(self, drivers, q10):
        return 1

    def partitioning_rules(self):
        self.partition_rules["above_structural"] = 1 - 0.45
        self.partition_rules["above_metabolic"] = 0.45

class Atmosphere(CarbonPool):

    def partitioning_rules(self):
        self.partition_rules["atmosphere"] = 1

class AboveMicrobe(CarbonPool):
    max_decay_rate = 0.3935

    def partitioning_rules(self):
        self.partition_rules["slow"] = 0.4
        self.partition_rules["atmosphere"] = 0.6

class SlowCarbon(CarbonPool):
    # TODO: can move to init or add some funciton to account for soil type 
    max_decay_rate = 0.0114315

    def partitioning_rules(self):
        self.partition_rules["passive"] = max(0, 0.003 - 0.009*PERCENT_CLAY)
        self.partition_rules["atmosphere"] = 0.55
        self.partition_rules["active"] = 1 - sum([x for x in self.partition_rules.values()])

class PassiveCarbon(CarbonPool):
    max_decay_rate = 0.0004

    def __init__(self, shape):
        super(PassiveCarbon, self).__init__(shape)
        self.carbon += 31 # seed pool to reach equilib faster

class ActiveCarbon(CarbonPool):
    max_decay_rate = 0.4557
    def partitioning_rules(self):
        self.partition_rules["passive"] = 0.003 + 0.032*PERCENT_CLAY
        self.partition_rules["atmosphere"] =  0.85 - 0.68*SILT_FRACTION 
        self.partition_rules["slow"] = 1 - sum([x for x in self.partition_rules.values()])

class CoarseWoodyDebris(CarbonPool):
    max_decay_rate = 0.02061782

    CWD_MICROBE_EFFICIENCY = 0.3
    def __init__(self, shape, metabolic_percent, max_decay_rate=None):   #TODO use *args?
        self.metabolic_percent = metabolic_percent
        super(CoarseWoodyDebris, self).__init__(shape, max_decay_rate)

    def partitioning_rules(self):
        self.partition_rules["atmosphere"] =  self.CWD_MICROBE_EFFICIENCY
        self.partition_rules["above_microbe"] = self.metabolic_percent *\
                (1 - self.CWD_MICROBE_EFFICIENCY)
        self.partition_rules["above_structural"] = (1 - self.metabolic_percent) *\
                (1 - self.CWD_MICROBE_EFFICIENCY)

def main():
    print "\nMain not implemented...\n"

if __name__ == "__main__":
    main()
