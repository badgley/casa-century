import argparse 
import calendar
import glob
import random
import traceback
import os
from os import path
import time
import json
import logging

import pandas as pd
from pandas.util.testing import set_trace
import numpy as np

from carbonpool import *

SPINUP = True

DEFAULT_AMOS = 0.55
SILT_FRACTION = 0.66 
PERCENT_CLAY = 0.8

logger = logging.getLogger("logger")
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
logger.addHandler(ch)

class Model(object):
    initial_month = 1
    metab_prc = 0.45

    def __init__(cls, carbon_pools, partition, year=1000, q10=1.5):
        cls.partition = partition
        cls.carbon_pools = carbon_pools
        cls.driver_data = {}
        cls.q10=q10
        cls.npp = {}
        cls.identity = time.time()
        cls.year = year
        
    def allocate_initial_carbon(cls, carbon, partition_percents=None):
        cls.initial_carbon = carbon
        cls.total_carbon = np.sum(carbon)
        if not partition_percents:
            partition_percents = {key: 1.0/len(cls.carbon_pools) for key in cls.carbon_pools}

        for pool, percent in partition_percents.iteritems():
            cls.carbon_pools[pool].input_carbon(carbon * percent)

    def _load_driver_data(cls, month):
        return (np.tile(35,10), np.tile(35,10), np.tile(35,10))

    def get_driver_data(cls, month):
        try:
            return cls.driver_data[month]
        except:
            cls.driver_data[month] = cls._load_driver_data(month)
            return cls.driver_data[month]

    def reallocate_carbon(cls, reallocations):
        for reallocation in reallocations:
            for pool, carbon in reallocation.iteritems():
                cls.carbon_pools[pool].input_carbon(carbon)

    def get_random_pool(cls):
        # TODO: remove carbon from names! should be more general 
        return random.choice(cls.carbon_pools.values())

    def c_to_amos(cls, reallocation):
        try:
            to_amos = reallocation['atmosphere'][~np.isnan(cls.mask)]
            return np.nansum(to_amos)
        except:
            return 0

    def run_pools(cls, driver_data):
        """ Decay carbon in all pools

        Pool runner executes the run command for every carbon pool -- 
        keeps track of how much carbon leaves that pool which then
        get reallocated appropriately. 

        """
        reallocations = []
        to_amos = []
        for name, pool in cls.carbon_pools.iteritems():
            carbon_out = pool.run(driver_data, q10=cls.q10, SPINUP=SPINUP)
            if carbon_out:
                reallocations.append(carbon_out)
                to_amos.append(cls.c_to_amos(carbon_out))

        global SPINUP
        if len(to_amos) > 0 and not SPINUP:
            cur_npp =  np.nansum(cls.npp[cls.current_month % 12 + 1][~np.isnan(cls.mask)])
            print "Emiited %.2f Mg to atmosphere with %.2f NPP" % (sum(to_amos), cur_npp)
        return reallocations

    def run(cls, start_month=0):
        """ step the model forward one timestep, including NPP calc and running pools
        """
        cls.current_month = start_month
        while cls.current_month < 300:
            month = cls.current_month % 12 + 1 # months stored 1-12 
            cls.grow(month)
            driver_data = cls.get_driver_data(month)
            reallocations = cls.run_pools(driver_data)
            cls.reallocate_carbon(reallocations)

            if len(reallocations) > 0:
                cls.calculate_nee(reallocations)
            else:
                set_trace()
            cls.current_month += 1
            
        cls.total_months = cls.current_month - start_month

    def summarize_pools(cls):
        """ Snapshot of carbon pools
        """
        with open("/".join(["data", "output", "%d_model_out.json" % cls.year]), "ab") as f:
            try:
                for name, pool in cls.carbon_pools.iteritems():
                    carbon = pool.carbon.drought.sum() / (1.0 * cls.total_carbon)
                    message = {
                            "variable": "percent_residual",
                            "month": cls.current_month,
                            "pool": name,
                            "value": "%.3f" % carbon
                            }
                    json.dump(message, f)
                    f.write("\n")

                    message = {
                            "variable": "reduction",
                            "month": cls.current_month,
                            "pool": name,
                            "value": 1
                            }
            except:
                traceback.print_exc()
                pass

    def calculate_nee(cls, pool_reallocations):
        """ Record relevant NEE metrics to logs
        """
        # TODO: go through logger
        c_to_amos = pd.DataFrame()
        for pool_reallocation in pool_reallocations:
            try:
                c_to_amos = pd.concat([c_to_amos, pool_reallocation['atmosphere']])
            except:
                pass
        c_to_amos = c_to_amos.groupby(c_to_amos.index).sum()
        c_to_amos = c_to_amos[~np.isnan(cls.mask)] 

        NPP = cls.npp[cls.current_month % 12 + 1]
        NPP = NPP[~np.isnan(cls.mask)] 

        drought_scenario = c_to_amos.base + c_to_amos.drought 

        try:
            # basinwide metrics
            drought_emissions = drought_scenario.sum() 
            base_emissions = c_to_amos.base.sum()

            base_case = base_emissions - np.nansum(NPP)
            drought_case = drought_emissions - np.nansum(NPP) 

            print "The current month is %d" % cls.current_month
            for name, pool in cls.carbon_pools.iteritems():
                try:
                    drought_carbon = pool.carbon.drought.sum()
                    per_drought = drought_carbon / np.nansum(cls.initial_carbon)
                    with open("data/output/%d_basin_wide_dist_of_drought_c" % cls.year, "ab") as f:
                        f.write("%d, %s,%.5f\n" % (cls.current_month, name, per_drought))
                except:
                    pass

            with open("data/output/%d_basinwide_reduction" % cls.year, "ab") as f:
                f.write("%.2f,%d,%.3f,%.3f,%.3f,%.3f\n" % (cls.identity, # model run ID
                                                           cls.current_month,
                                                           base_case,
                                                           drought_case,
                                                           np.nansum(NPP),
                                                           drought_case - base_case))
                        
            # geospecific logging
            fname = "data/output/%d_geospecific_reduction" % cls.year
            
            drought_nee = drought_scenario - NPP 
            base_nee = c_to_amos.base - NPP 
            prc_reduction = (drought_nee - base_nee) / base_nee
            df = pd.concat([base_nee, drought_nee, prc_reduction], axis=1)
            df['month'] = cls.current_month
            df.to_csv(fname, mode='a', header=False) 

        except:
            traceback.print_exc()
            set_trace()
            time.sleep(2)
    
    def spinup_debug(cls):
        """ Display mean of carbon pools 
        """
        for name, pool in cls.carbon_pools.iteritems():
            try:
                val = np.nanmean(pool.carbon[~np.isnan(cls.mask)])
                print "%s has %.2f Mg carbon" % (name, val)
            except:
                print "loading mask"
                cls.mask = np.loadtxt(os.path.dirname(os.path.abspath(__file__)) +\
                        "/data/init_amz_carbon")
                traceback.print_exc()

class AmazonModel(Model):

    def grow(cls, month, monthly_allocation=None):
        try: 
            npp = cls.npp[month]
        except:
            gpp_fname = "/data/cru/gpp/monthly_climatology_%d_2001_2010" % month 
            gpp = np.loadtxt(os.path.dirname(os.path.abspath(__file__)) + gpp_fname)
            
            # TODO: make units explicit in the model somehow?
            days = calendar.monthrange(2001, month)[1] 
            npp = (gpp * 10 * 60 * 60 * 24 * days) / 2 # NPP = GPP / 2;  convert from kg m-2 s-1 
            cls.npp[month] = npp
            npp = cls.npp[month]

        cls.carbon_pools["living_root"].input_carbon(npp * cls.partition["root"]) 
        cls.carbon_pools["living_leaf"].input_carbon(npp * cls.partition["leaf"]) 
        cls.carbon_pools["living_wood"].input_carbon(npp * cls.partition["cwd"]) 

    def spinup(cls):
        """ Run model until satisfy equilibrium condition
        """
        global SPINUP
        SPINUP = True

        i = 0

        cls.mask = np.loadtxt(os.path.dirname(os.path.abspath(__file__)) + "/data/init_amz_carbon")
        while i < 9600: 
            month = i % 12 + 1 # months stored 1-12 
            cls.spinup_month = month

            cls.grow(month)

            driver_data = cls.get_driver_data(month)
            reallocations = cls.run_pools(driver_data)
            cls.reallocate_carbon(reallocations)
            
            if i % 400 == 0:
                print "Spinup achieved %d iterations" % i
                cls.spinup_debug()
            i += 1

            # LEFT OVER FROM CALIBRATION
                #cwd_mean = np.nanmean(cls.carbon_pools['cwd'].carbon[~np.isnan(cls.mask)])
                #slow_mean = np.nanmean(cls.carbon_pools['slow'].carbon[~np.isnan(cls.mask)])
                #abv_str_mean = np.nanmean(
                #                  cls.carbon_pools['above_structural'].carbon[~np.isnan(cls.mask)])
                #if ((cwd_mean > 32 or cwd_mean < 17) and
                #    i > 300 or slow_mean > 120 or abv_str_mean > 20):
                #    print "abandoning spinup after %d iter because cwd out of equilib" % i
                #    return 0

        for pool in cls.carbon_pools.values():
            pool.carbon = pd.DataFrame({'base': pool.carbon})

        global SPINUP
        SPINUP = False
        return 1

    def _load_driver_data(cls, month):
        """ Loads temp/precip/pet data into model 
        """
        base_path = path.abspath("./data/cru/")
        drivers = ["temp", "precip", "pet"] 
        data = []
        for driver in drivers:
            f = glob.glob(base_path + "/%s/" % driver + "*_%d_*" % month)[0]
            data.append(np.loadtxt(f))
        return tuple(data)

    def allocate_drought_carbon(cls, carbon, partition_percents=None):
        cls.initial_carbon = carbon
        cls.total_carbon = np.nansum(carbon)
        if not partition_percents:
            partition_percents = {"cwd": 0.39,
                                  "leaf": 0.34,
                                  "root": 0.27,}

        cwd_carbon = pd.DataFrame({"drought": carbon * partition_percents["cwd"]})
        leaf_carbon = pd.DataFrame({"drought": carbon * partition_percents["leaf"]})
        root_carbon = pd.DataFrame({"drought": carbon * partition_percents["root"]})

        # TODO: should i take the carbon OUT from the living pool? 
        cls.carbon_pools["cwd"].input_carbon(cwd_carbon)

        cls.carbon_pools["root_structural"].input_carbon(root_carbon * (1 - cls.metab_prc))
        cls.carbon_pools["root_metabolic"].input_carbon(root_carbon * cls.metab_prc)

        cls.carbon_pools["above_structural"].input_carbon(leaf_carbon * (1 - cls.metab_prc))
        cls.carbon_pools["above_metabolic"].input_carbon(leaf_carbon * cls.metab_prc)

    def allocate_belowground(cls, carbon):
        cls.carbon_pools["root_structural"].input_carbon(carbon * (1 - cls.metab_prc))
        cls.carbon_pools["root_metabolic"].input_carbon(carbon * cls.metab_prc)

    def allocate_aboveground(cls, carbon):
        cls.carbon_pools["above_structural"].input_carbon(carbon * (1 - cls.metab_prc))
        cls.carbon_pools["above_metabolic"].input_carbon(carbon * cls.metab_prc)

def setup_model_run(year, params=None):
    """ Set up pools/params for model

    this extra level of abstraction means that you can multiprocess -- which havent used 
    for experiment...hmmm
    """
    if not params:
        # BEST
        params = {
                "q10": random.choice([1.6, 1.8]), # 1.8
                "slow_decay": random.choice(np.arange(0.008, 0.0094, 0.0002)), # 0.0092
                "cwd_decay": random.choice(np.arange(0.0183, 0.02, 0.001)), # 0.0183
                "percent_cwd": random.choice([0.35, 0.42]),
                "abv_str_decay": random.choice([0.5, 0.52, 0.54]),
                }

        METABOLIC_PERCENT = 0.45
        shape = (10304,)

        carbon_pools = {
                "atmosphere": Atmosphere(shape),
                "cwd": CoarseWoodyDebris(shape, METABOLIC_PERCENT,
                                         max_decay_rate=params['cwd_decay']),
                "active": ActiveCarbon(shape),
                "slow": SlowCarbon(shape, max_decay_rate=params['slow_decay']),
                "passive": PassiveCarbon(shape),
                "root_metabolic": RootMetabolic(shape),
                "root_structural": RootStructural(shape),
                "above_microbe": AboveMicrobe(shape),
                "above_structural": AboveStructural(shape),
                "above_metabolic": AboveMetabolic(shape),
                "living_wood": LivingWood(shape),
                "living_leaf": LivingLeaf(shape),
                "living_root": LivingRoot(shape),
                }

        partition = {
                "root": 0.27,
                "cwd": params['percent_cwd'],
                "leaf": 1 - params['percent_cwd'] - 0.27,}

        model = AmazonModel(carbon_pools, partition, year=year, q10=params['q10'])
        model.spinup()
        return model

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--year", help="model year to run", default=1000, type=int)
    parser.add_argument("--path", help="output path", default="/tmp")
    parser.add_argument("--nrun", help="output path", default=10, type=int)

    return parser.parse_args()

def main():
    args = parse_args()
    carbon = np.loadtxt("data/%d_init_amz_carbon" % args.year)
    initial_carbon = np.abs(carbon) 

    run = 0

    while run < args.nrun:
        model = setup_model_run(args.year)
        model.allocate_drought_carbon(initial_carbon)
        model.run()
        run += 1

if __name__ == "__main__":
    main()
