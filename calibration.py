
def model_calibration(pid):

    while True:
        params = param_generator()
        METABOLIC_PERCENT = 0.45
        shape = (10304,)
        carbon_pools = {
                "atmosphere": CarbonPool(shape),
                "cwd": CoarseWoodyDebris(shape, METABOLIC_PERCENT, 
                                         max_decay_rate=params['cwd_decay']),
                "active": ActiveCarbon(shape),
                "slow": SlowCarbon(shape, max_decay_rate=params['slow_decay']),
                "passive": PassiveCarbon(shape),
                "above_structural": AboveStructural(shape,
                                                    max_decay_rate=params["abv_str_decay"]),
                "above_metabolic": AboveMetabolic(shape),
                "above_microbe": AboveMicrobe(shape),
                "root_metabolic": RootMetabolic(shape),
                "root_structural": RootStructural(shape),
                }
        partition = {"root": 0.27, "cwd": params['percent_cwd'], "leaf": 1 - params['percent_cwd']}
        model = AmazonModel(carbon_pools, partition, q10=params['q10'])
        status = model.spinup()
        if status:
            print "Process %d writing results" % pid
            with open("/tmp/gpp_model_calibration.json", "ab") as f:
                for name, pool in model.carbon_pools.iteritems():
                    message = {k:v for k, v in params.iteritems()}
                    message['pool'] = name
                    carbon = np.nanmean(pool.carbon[~np.isnan(model.mask)])
                    message['carbon'] = "%.3f" % carbon
                    json.dump(message, f)
                    f.write("\n")

def calibration():
    import multiprocessing
    workers = multiprocessing.Pool(8)
    workers.map(model_calibration, range(8))
