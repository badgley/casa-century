import pandas as pd
def crosser(data, col_name='prc_d', thold=0.05):
    gt_thold = data[col_name].apply(abs) > thold
    lt_thold = data[col_name].apply(abs) <= thold
    try:
        last_exceed = data[gt_thold].tail(1).month.values[0]
    except:
        return (1, 1) # no drought loss
    try:
        first_cross = data[lt_thold].head(1).month.values[0]
    except:
        first_cross = last_exceed
    if last_exceed > first_cross:
        final_cross = last_exceed
    else:
        final_cross = first_cross
    return (first_cross, final_cross)

def get_data(fname):
    df = pd.read_csv(fname ,names=['location','base','drought', 'reduction', 'month'])
    df = df.groupby(['location', 'month']).mean()
    df.reset_index(inplace=True)
    df.ix[(df.drought - df.base).abs() < 0.01, 'drought'] = df.ix[(df.drought - df.base).abs() < 0.01, 'base']
    df['prc_d'] = (df['drought'] - df['base']) / df['base']
    thresh_crosses = df.groupby('location').apply(crosser)
    df['first'] = thresh_crosses.apply(lambda x: x[0])
    df['final'] = thresh_crosses.apply(lambda x: x[1])
    return df

def rasterize(data):
    template = pd.read_csv("/home/darryl/src/amzrec/data/init_amz_carbon",names=['this']) 
    return template.join(data)

def net_sink(data, return_val='yr'):
    if sum(data.base < 0) != 0:
        drought_case = data[data.drought < 0].head(1)[return_val].values[0]
    else:
        drought_case =  pd.np.nan
    return drought_case

def main():
    fname = "/home/darryl/src/amzrec/data/output/geospecific_reduction"
    df = get_dta(fname)
    crosses = df.groupby('location').apply(functs.crosser) 
    crosses = pd.concat([crosses.apply(lambda x: x[0]), crosses.apply(lambda x: x[1])], axis=1) 
    crosses = crosses.rename(columns={0:'first',1:'final'})

if __name__ == "__main__":
    main()

