import os
from IPython.core.getipython import get_ipython
import luigi
import pandas as pd
import numpy as np

# TODO: deal with partial years
# TODO: determine the resolution before writing

ip = get_ipython()

class RawData(luigi.ExternalTask):

    fname = luigi.Parameter()

    def output(self):
        return luigi.LocalTarget(self.fname)

class MonthlyClimatology(luigi.Task):
    """ Takes a date range and ... a file path? 
    taken from casa -- merge back someday i guess..
    """
    start_year = luigi.IntParameter(default=2001)
    end_year = luigi.IntParameter(default=2010)
    month = luigi.IntParameter()
    
    def output(self):
        file_path = self.path_name + "/monthly_climatology_%d_%d_%d" % (self.month,
                                                                         self.start_year,
                                                                         self.end_year)
        return luigi.LocalTarget(file_path)

    def run(self):
        """ Omg...
        """
        ip.magic("load_ext rmagic") # let the chaos begin
        
        amazon_carbon = os.path.dirname(os.path.abspath(__file__)) + "/data/Drought_2010_Carbon_Loss_ABG_Mg_ha.tif"
        ip.magic("R fname <- '%s'" % self.input().path)
        ip.magic("R amazon <- '%s'" % amazon_carbon)
        ip.magic("R library(raster)")
        ip.magic("R amazon <- raster(amazon)")

        ip.magic("R amazon <- aggregate(amazon, 2)") # resample to hd
        ip.magic("R cru_raster <- stack(fname)")
        ip.magic("R monthly <- subset(cru_raster, seq(%d,60,12))" % self.month)
        ip.magic("R monthly <- crop(monthly, extent(amazon))")
        ip.magic("R print(monthly)")
        monthly_mean = ip.magic("R getValues(mean(monthly))")
        with self.output().open("w") as out_f:
            np.savetxt(out_f, monthly_mean)

class PrecipClimatology(MonthlyClimatology):
    data_source = "/Users/grayson/Downloads/cru_ts3.21.2001.2010.pre.dat.nc"
    path_name = os.path.dirname(os.path.abspath(__file__)) + "/data/cru/precip" 
    def requires(self):
        return RawData(self.data_source)

class TempClimatology(MonthlyClimatology):
    data_source = "/Users/grayson/Downloads/cru_ts3.21.2001.2010.tmp.dat.nc"
    path_name = os.path.dirname(os.path.abspath(__file__)) + "/data/cru/temp" 
    def requires(self):
        return RawData(self.data_source)

class GPPClimatology(MonthlyClimatology):
    data_source = os.path.dirname(os.path.abspath(__file__)) + "/data/subset_mpi_gpp.nc"
    path_name = os.path.dirname(os.path.abspath(__file__)) + "/data/cru/gpp"

    def requires(self):
        return RawData(self.data_source)


class PETClimatology(MonthlyClimatology):
    data_source = "/Users/grayson/Downloads/cru_ts3.21.2001.2010.pet.dat.nc"
    path_name = os.path.dirname(os.path.abspath(__file__)) + "/data/cru/pet" 
    def requires(self):
        return RawData(self.data_source)

class AmazonData(luigi.Task):
    def requires(self):
        tasks = []
        for month in range(1,13):
            tasks = tasks + [PrecipClimatology(month=month),
                     TempClimatology(month=month),
                     PETClimatology(month=month),
                     GPPClimatology(month=month),
                     ]
        return tasks

if __name__ == "__main__":
    luigi.run()
